#!/bin/bash

mkdir -p $PREFIX/bin
mkdir -p $PREFIX/dat

mv reduce_src/reduce_wwPDB_het_dict.txt $PREFIX/dat
sed -i "s|DICT_HOME   =/usr/local/reduce_wwPDB_het_dict.txt|DICT_HOME   =$PREFIX/dat/reduce_wwPDB_het_dict.txt|" reduce_src/Makefile

make clean
make
mv reduce_src/reduce $PREFIX/bin
